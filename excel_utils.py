# coding: utf-8
"""
Utilities for writing results to excel files.
"""

import xlsxwriter
import cv2
import imutils

_PHONE_OMR_FIELD_NAME = 'Phone OMR'


def write_to_excel(all_detected_text, all_detected_markers, template_info, output_excel_file_path):
    """
    Write information in detected_text and detected_markers to excel file, following the template specified
    in template_info.
    :param all_detected_text:
    :param all_detected_markers:
    :param template_info:
    :param output_excel_file_path:
    :return:
    """
    # get unique header names in order of appearance in template
    header_names = list(set([t[1] for t in template_info]))

    # open excel workbook
    workbook = xlsxwriter.Workbook(output_excel_file_path)
    worksheet = workbook.add_worksheet()

    # write header names
    for i in range(len(header_names)):
        worksheet.write(0, i, header_names[i])

    for row_no in range(len(all_detected_text)):
        detected_text = all_detected_text[row_no]
        detected_markers = all_detected_markers[row_no]

        # get values by attribute (in order of appearance)
        text_counter = 0
        marker_counter = 0
        attributes = ['']*len(header_names)
        for i in range(len(template_info)):
            info_cat, attr, attr_val = template_info[i]
            # if text category
            if info_cat == 'T':
                attributes[header_names.index(attr)] = detected_text[text_counter]
                text_counter += 1
            # if marker category or the marker part of a marker+text category
            elif info_cat == 'M' or (info_cat == 'M+T' and len(attr_val) > 0):
                # if marker circle was filled
                if marker_counter == len(detected_markers):
                    break
                if detected_markers[marker_counter] is True:
                    # if already filled with some previous value, append
                    if len(attributes[header_names.index(attr)]) > 0:
                        attributes[header_names.index(attr)] = attributes[header_names.index(attr)] + ' ' + attr_val
                    else:
                        attributes[header_names.index(attr)] = attr_val
                marker_counter += 1
            # if text part of a marker+text category
            elif info_cat == 'M+T' and len(attr_val) == 0:
                if len(attributes[header_names.index(attr)]) > 0:
                    attributes[header_names.index(attr)] = attributes[header_names.index(attr)] + ' ' \
                                                           + detected_text[text_counter]
                else:
                    attributes[header_names.index(attr)] = detected_text[text_counter]
                text_counter += 1
        for i in range(len(header_names)):
            worksheet.write(row_no+1, i, attributes[i])
    workbook.close()


def write_to_excel_with_images(all_detected_text, all_detected_markers, template_info, output_excel_file_path):
    """
    Write information in detected_text and detected_markers to excel file, following the template specified in
    template_info (detected_text is assumed to contain images)
    :param all_detected_text:
    :param all_detected_markers:
    :param template_info:
    :param output_excel_file_path:
    :return:
    """
    # get unique header names in order of appearance in template
    all_header_names = [t[1] for t in template_info]
    header_names = [all_header_names[0]]
    phone_done = False
    for i in range(len(all_header_names)-1):
        # combine 'Ph1', 'Ph2', etc. into 'Phone OMR'
        if (len(all_header_names[i+1]) == 3 or len(all_header_names[i+1]) == 4) \
                and all_header_names[i+1].startswith('Ph'):
            if not phone_done:
                header_names.append(_PHONE_OMR_FIELD_NAME)
                phone_done = True
        elif all_header_names[i+1] not in header_names:
            header_names.append(all_header_names[i+1])

    # open excel workbook
    workbook = xlsxwriter.Workbook(output_excel_file_path)
    worksheet = workbook.add_worksheet()

    # set header to bold
    header_format = workbook.add_format({'bold': True})
    worksheet.set_row(0, None, header_format)
    # set row height to 20
    for i in range(len(all_detected_text)):
        worksheet.set_row(i+1, 50)

    # write header names
    [worksheet.write(0, i, header_names[i]) for i in range(len(header_names))]

    for row_no in range(len(all_detected_text)):
        # resize all text images
        for j in range(len(all_detected_text[row_no])):
            im = cv2.imread(all_detected_text[row_no][j])
            im = imutils.resize(im, width=500, height=160)
            cv2.imwrite(all_detected_text[row_no][j], im)

        detected_text = all_detected_text[row_no]
        detected_markers = all_detected_markers[row_no]

        # get values by attribute (in order of appearance)
        text_counter = 0
        marker_counter = 0
        attributes = ['']*len(header_names)
        attribute_categories = ['']*len(header_names)
        # initialize attribute and attribute category for phone OMR
        attributes[header_names.index(_PHONE_OMR_FIELD_NAME)] = '0'*10
        attribute_categories[header_names.index(_PHONE_OMR_FIELD_NAME)] = 'M'
        for i in range(len(template_info)):
            info_cat = template_info[i][0]
            attr = template_info[i][1]
            attr_val = template_info[i][2]
            # if text category
            if info_cat == 'T':
                attribute_categories[header_names.index(attr)] = 'T'
                attributes[header_names.index(attr)] = detected_text[text_counter]
                text_counter += 1
            # if marker category or the marker part of a marker+text category
            elif info_cat == 'M' or (info_cat == 'M+T' and len(attr_val) > 0):
                # if marker circle was filled
                if marker_counter == len(detected_markers):
                    break
                # if it is the marker that indicates text field input in case of Marker+Text
                if detected_markers[marker_counter] is True:
                    # if Marker+Text's text indicator marker
                    if len(template_info[i]) == 4 and template_info[i][3] == 'T' and info_cat == 'M+T':
                        attribute_categories[header_names.index(attr)] = 'M+T'
                    # attribute category has already been set once for _PHONE_OMR_FIELD_NAME
                    elif not (attr.startswith('Ph') and (len(attr) == 3 or len(attr) == 4)):
                        attribute_categories[header_names.index(attr)] = 'M'
                    # if phone OMR
                    if attr.startswith('Ph') and (len(attr) == 3 or len(attr) == 4):
                        attributes[header_names.index(_PHONE_OMR_FIELD_NAME)] = attributes[header_names.index(_PHONE_OMR_FIELD_NAME)][:int(''.join(attr[2:]))-1] + attr_val + attributes[header_names.index(_PHONE_OMR_FIELD_NAME)][int(''.join(attr[2:])):]
                    # if already filled with some previous value, append
                    elif len(attributes[header_names.index(attr)]) > 0:
                        attributes[header_names.index(attr)] = attributes[header_names.index(attr)] + ' ' + attr_val
                    else:
                        attributes[header_names.index(attr)] = attr_val
                marker_counter += 1
            # if text part of a marker+text category
            elif info_cat == 'M+T' and len(attr_val) == 0:
                if attribute_categories[header_names.index(attr)] == 'M+T':
                    attribute_categories[header_names.index(attr)] = 'T'
                    attributes[header_names.index(attr)] = detected_text[text_counter]
                else:
                    attribute_categories[header_names.index(attr)] = 'M'
                text_counter += 1
        for i in range(len(header_names)):
            if attribute_categories[i] == 'M':
                worksheet.write(row_no+1, i, attributes[i])
                worksheet.set_column(i, i, max(len(header_names[i]), len(attributes[i])))
            elif attribute_categories[i] == 'T':
                worksheet.insert_image(row_no+1, i, attributes[i])
                worksheet.set_column(i, i, max(len(header_names[i]), 75))
    workbook.close()
