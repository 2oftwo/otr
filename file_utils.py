# coding: utf-8
"""
Utilities for managing files.
"""
import os


def upload_files(received_files, upload_dir_path):
    """
    Upload all received files to the specified upload directory
    :param received_files:
    :param upload_dir_path:
    :return:
    """
    for received_file in received_files:
        filename = received_file['filename']
        # replace '/' to prevent accessing server's file system
        filename = filename.replace("/", "")
        with open(os.path.join(upload_dir_path, filename), "w") as fout:
            fout.write(received_file['body'])
