# coding: utf-8
"""
Utilities for extraction of optical marks and text in documents using image processing libraries.
"""
# import the necessary packages
import cv2
import imutils
from imutils import contours
from imutils.perspective import four_point_transform
from PIL import Image
import pytesseract
import sys
import template_utils
import excel_utils
import os
import os.path
import glog as log

_TEMP_IMAGES_DIR_PATH = 'external_files/images/temp'
_OUTPUT_IMAGES_DIR_PATH = 'external_files/images/output'
if not os.path.exists(_TEMP_IMAGES_DIR_PATH):
    os.makedirs(_TEMP_IMAGES_DIR_PATH)
if not os.path.exists(_OUTPUT_IMAGES_DIR_PATH):
    os.makedirs(_OUTPUT_IMAGES_DIR_PATH)

PAPER_HEIGHT_FOR_EDGE = 1200
MIN_MARKER_WIDTH = 19
MAX_MARKER_WIDTH = 34
MIN_MARKER_HEIGHT = 19
MAX_MARKER_HEIGHT = 34
MIN_TEXT_WIDTH = 100
MIN_TEXT_HEIGHT = 20
MIN_AREA = 150
MAX_AREA = 750

MAX_HORIZONTAL_LINE_DEVIATION = 10

DEFAULT_THRESHOLD_PERC = 50


def preprocess_image(image_file_path):
    """
    Preprocess image by doing the following:
    1) convert to grayscale
    2) smooth the image using Gaussian blur
    3) detect edges in the image using Canny algorithm
    4) find the largest contour in the edge-detected image (to detect the paper)
    5) use four-point-transform on the paper for top-down view
    6) binarize the image using Otsu's thresholding algorithm
    :param image_file_path:
    :return:
    """
    errors = ''
    image = cv2.imread(image_file_path)
    orig_image = image.copy()
    #ratio = [1.0*orig_image.shape[0]/image.shape[0],1.0*orig_image.shape[1]/image.shape[1]]
    image = imutils.resize(image, height=PAPER_HEIGHT_FOR_EDGE)
    # convert the image to grayscale, blur it, and find edges
    # in the image
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    #blur = cv2.GaussianBlur(gray, (25, 25), 0)
    edged = cv2.Canny(gray, 75, 200)
    cv2.imwrite(_TEMP_IMAGES_DIR_PATH + '/edged.png', edged)
    # find contours in the edge-detected image
    cnts = cv2.findContours(edged, cv2.RETR_LIST,
                            cv2.CHAIN_APPROX_SIMPLE)
    # opencv v2.4.X's output is different from v3
    cnts = cnts[0] if imutils.is_cv2() else cnts[1]
    # ensure that at least one contour was found
    if len(cnts) == 0:
        errors += '<br><br>No paper found in image file: ' + image_file_path.split('/')[-1] \
                  + '<br>Check if <br>(1) all four corners of paper are visible in the image and <br>(2) the image ' \
                    'has a contrasting black background.'
        return None, None, None, errors

    # get the coordinates of the contour of the paper
    cnt_coords = None
    # sort the contours according to their size in
    # descending order
    cnts = sorted(cnts, key=cv2.contourArea, reverse=True)
    # loop over the sorted contours
    for c in cnts:
        # approximate the contour
        (x, y, w, h) = cv2.boundingRect(c)
        log.debug('contour: %d %d %d %d' % (x, y, w, h))
        # paper contour should have minimum width and height
        peri = cv2.arcLength(c, True)
        approx = cv2.approxPolyDP(c, 0.1 * peri, True)
        # if our approximated contour has four points,
        # then we can assume we have found the paper
        if len(approx) == 4:
            cnt_coords = approx
            break
    # apply a four point perspective transform to the grayscale image to obtain
    # a top-down view of the paper
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    warped = four_point_transform(gray, cnt_coords.reshape(4, 2))
    cv2.imwrite(_TEMP_IMAGES_DIR_PATH + '/warped.png', warped)
    # adaptive Gaussian thresholding is able to detect edges of OMR circles and text boxes (but this is not good
    # for detecting shading inside OMR circles)
    thresh = cv2.adaptiveThreshold(warped, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY_INV, 25, 2)
    cv2.imwrite(_TEMP_IMAGES_DIR_PATH + '/thresh.png', thresh)
    # Otsu thresholding is able to capture shading of circles but not circle or text box edges; so, this can be
    # used for detecting selected OMR circles
    thresh2 = cv2.threshold(warped, 0, 255, cv2.THRESH_BINARY_INV | cv2.THRESH_OTSU)[1]
    cv2.imwrite(_TEMP_IMAGES_DIR_PATH + '/thresh2.png', thresh2)
    return warped, thresh, thresh2, errors


def detect_text_and_omr_locations(preprocessed_image):
    """
    Detect the locations of text and OMR in the survey.    
    :param preprocessed_image: 
    :return: 
    """
    cnts = cv2.findContours(preprocessed_image, cv2.RETR_EXTERNAL,
                            cv2.CHAIN_APPROX_SIMPLE)
    cnts = cnts[0] if imutils.is_cv2() else cnts[1]
    image_width = len(preprocessed_image[0])
    image_height = len(preprocessed_image)
    #cnts = contours.sort_contours(cnts, method='top-to-bottom')[0] # not
    #needed since sorting is being done both by top-to-bottom and left-to-right
    #at a later stage
    # detect text rectangle box and OMR circle contours
    # get text and marker contours (for debugging)
    text_cnts = []
    marker_cnts = []
    # get text and marker bounding box locations (to return)
    text_locations = []
    marker_locations = []
    # loop over the contours
    for c in cnts:
        # compute the bounding box of the contour, then use the
        # bounding box to derive the aspect ratio
        (x, y, w, h) = cv2.boundingRect(c)
        # ignore very small regions or regions at edges of the page
        if w < min(MIN_MARKER_WIDTH, MIN_TEXT_WIDTH) or h < min(MIN_MARKER_HEIGHT, MIN_TEXT_HEIGHT):
            continue
        area = cv2.contourArea(c)
        if x <= 75 or x >= image_width-75 or y <= 75 or y >= image_height-75:
            continue
        ar = w / float(h)
        # approximate the contour
        peri = cv2.arcLength(c, True)
        approx = cv2.approxPolyDP(c, 0.01 * peri, True)
        log.debug('location: %d %d %d %d; approx: %s' % (x, y, w, h, approx))
        log.debug('area: %.2f' % area)
        # in order to label the contour as an OMR circle,
        # 1) region should be sufficiently wide, sufficiently tall,
        # 2) have an aspect ratio approximately equal to 1, and
        # 3) shape should have have more than 4 vertices (assuming that there
        # are no other polygons in the image)
        if w >= MIN_MARKER_WIDTH and w<=MAX_MARKER_WIDTH and h >= MIN_MARKER_HEIGHT and h <= MAX_MARKER_HEIGHT \
                and ar >= 0.7 and ar <= 1.3 and len(approx) > 4 and area >= MIN_AREA and area <= MAX_AREA:
            marker_cnts.append(c)
            marker_locations.append((x, y, w, h))
        # in order to label the contour as an text rectangle,
        # 1) region should be sufficiently wide, sufficiently tall,
        # 2) have an aspect ratio approximately equal to 1:n s.t. n>=2, and
        # 3) shape should have 4 vertices (<10 vertices to accommodate for errors)
        elif w >= MIN_TEXT_WIDTH and h >= MIN_TEXT_HEIGHT and ar >= 5.0 and len(approx) < 10:
            text_cnts.append(c)
            text_locations.append((x, y, w, h))

    # sort both text and marker locations first from top to bottom and then
    # from left to right (note that this is different from (x,y) tuple sorting)
    # sort only if number of locations is 2 or more
    text_locations_by_line = text_locations
    if len(text_locations) > 1:
        # sort from top to bottom
        text_locations = sorted(text_locations, key=lambda k: k[1])
        # get number of text locations per line
        no_text_loc_per_line = [1]
        prev = 0
        for i in range(len(text_locations)-1):
            if text_locations[i+1][1]-text_locations[i][1] <= MAX_HORIZONTAL_LINE_DEVIATION:
                no_text_loc_per_line[prev] += 1
            else:
                no_text_loc_per_line.append(1)
                prev += 1
        # set text locations of first line
        text_locations_by_line = sorted(text_locations[:no_text_loc_per_line[0]], key=lambda k: k[0])
        # set text locations of other lines
        prev_no = no_text_loc_per_line[0]
        for i in range(len(no_text_loc_per_line)-1):
            text_locations_by_line.extend(sorted(text_locations[prev_no:prev_no+no_text_loc_per_line[i+1]],
                                                 key=lambda k: k[0]))
            prev_no += no_text_loc_per_line[i+1]

    marker_locations_by_line = marker_locations
    if len(marker_locations) > 1:
        # sort from top to bottom
        marker_locations = sorted(marker_locations, key=lambda k: k[1])
        # get number of text locations per line
        no_marker_loc_per_line = [1]
        prev = 0
        for i in range(len(marker_locations)-1):
            if marker_locations[i+1][1]-marker_locations[i][1] <= MAX_HORIZONTAL_LINE_DEVIATION:
                no_marker_loc_per_line[prev] += 1
            else:
                no_marker_loc_per_line.append(1)
                prev += 1
        # set text locations of first line
        marker_locations_by_line = sorted(marker_locations[:no_marker_loc_per_line[0]], key=lambda k: k[0])
        # set text locations of other lines
        prev_no = no_marker_loc_per_line[0]
        for i in range(len(no_marker_loc_per_line)-1):
            marker_locations_by_line.extend(sorted(marker_locations[prev_no:prev_no+no_marker_loc_per_line[i+1]],
                                                   key=lambda k: k[0]))
            prev_no += no_marker_loc_per_line[i+1]
    # debugging: labeling detected contours on the binarized image
    # make clone of thresholded image (for labeling text contours)
    clone = preprocessed_image.copy()
    # loop over the text contours and label them
    for (i, c) in enumerate(text_cnts):
        sorted_image = contours.label_contour(clone, c, i, color=(240, 0, 159))
        cv2.imwrite(_OUTPUT_IMAGES_DIR_PATH + '/text-contours.png', sorted_image)

    # make another clone of thresholded image (for labeling marker contours)
    clone2 = preprocessed_image.copy()
    # loop over the marker contours and label them
    for (i, c) in enumerate(marker_cnts):
        sorted_image = contours.label_contour(clone2, c, i, color=(240, 0, 159))
        cv2.imwrite(_OUTPUT_IMAGES_DIR_PATH + '/marker-contours.png', sorted_image)

    clone3 = preprocessed_image.copy()
    # loop over all contours and label them
    for (i, c) in enumerate(cnts):
        (x, y, w, h) = cv2.boundingRect(c)
        if w > 30 and h > 30:
            sorted_image = contours.label_contour(clone3, c, i, color=(240, 0, 159))
            cv2.imwrite(_OUTPUT_IMAGES_DIR_PATH + '/all-contours.png', sorted_image)

    return text_locations_by_line, marker_locations_by_line


def extract_written_text_as_image(image_file_name, image, text_locations):
    """
    Extract written text locations as images.
    :param image_file_name:
    :param image:
    :param text_locations:
    :return:
    """
    written_text_images = []
    for location in text_locations:
        x, y, w, h = location
        cropped_image = image[y:y+h, x:x+w]
        temp_image_path = _TEMP_IMAGES_DIR_PATH + '/'\
                          + image_file_name + '-temp-cropped-' + str(x) + '-' + str(y) + '.png'
        cv2.imwrite(temp_image_path, cropped_image)
        written_text_images.append(temp_image_path)
    return written_text_images


def detect_written_text(preprocessed_image, text_locations):
    """
    Detect text written within the text locations (x,y,w,h) of the preprocessed image
    using pytesseract library.
    :param preprocessed_image: 
    :param text_locations: 
    :return: 
    """
    detected_text = []
    for location in text_locations:
        x, y, w, h = location
        cropped_image = preprocessed_image[y:y+h, x:x+w]
        temp_image_path = _TEMP_IMAGES_DIR_PATH + '/temp-cropped-' + str(x) + '-' + str(y) + '.png'
        cv2.imwrite(temp_image_path, cropped_image)
        detected_text.append(pytesseract.image_to_string(Image.open(temp_image_path)))
    return detected_text


def detect_selected_markers(preprocessed_image, marker_locations, threshold_perc=DEFAULT_THRESHOLD_PERC):
    """
    Detect whether the marker in marker locations (x,y,w,h) of the preprocessed image
    are selected or not (True/False). Specify threshold percentage (default: 50%) for portion of the marker
    that needs to be shaded in order to consider as selected.    
    :param preprocessed_image: 
    :param marker_locations: 
    :param threshold_perc: 
    :return: 
    """
    selected_markers = []
    for location in marker_locations:
        x, y, w, h = location
        cropped_image = preprocessed_image[y:y+h, x:x+w]
        shaded_pixels = cv2.countNonZero(cropped_image)
        total_pixels = w*h
        shaded_perc = (100.0*shaded_pixels)/total_pixels
        selected_markers.append(shaded_perc >= threshold_perc)
    return selected_markers


def extract_omr_text(images_dir_path, template_file_path, output_excel_file_path):
    """
    Extract the optical marks and text from the documents and output to the specified result excel files.
    :param images_dir_path:
    :param template_file_path:
    :param output_excel_file_path:
    :return:
    """
    # read template file for reading sequence of text boxes and OMR markers
    count_correct_surveys = 0
    template_info, count_text, count_markers, count_pages, e = template_utils.extract_info(template_file_path)
    if len(e) > 0:
        return e, count_correct_surveys # count correct surveys will be 0
    image_file_paths = sorted([os.path.join(images_dir_path, f) for f in os.listdir(images_dir_path)
                               if os.path.isfile(os.path.join(images_dir_path, f))])
    all_detected_text = []
    all_detected_markers = []
    curr_detected_text = []
    curr_detected_markers = []
    counter = 1
    errors = ''
    for image_file_path in image_file_paths:
        if not image_file_path.lower().endswith('.png') and not image_file_path.lower().endswith('.jpg') \
                and not image_file_path.lower().endswith('.jpeg'):
            continue
        log.info(image_file_path)
        # preprocess image
        warped_image, preprocessed_image, preprocessed_image2, e = preprocess_image(image_file_path)
        if len(e) > 0:
            errors += e
            continue
        # extract text and marker locations from preprocessed image
        text_locations, marker_locations = detect_text_and_omr_locations(preprocessed_image)
        # detect written text in text locations as images (until handwriting recognition is implemented)
        # unique image name is needed as input to extract_written_text_as_image()
        image_file_name = image_file_path.split('/')[-1]
        detected_text = extract_written_text_as_image(image_file_name, warped_image, text_locations)
        # detect written text (using handwriting recognition)
        # detected_text = omr_text_utils.detect_written_text(cv2.imread(image_file_path),text_locations)
        # detect selected markers in marker circles
        detected_markers = detect_selected_markers(preprocessed_image2, marker_locations)
        log.debug(detected_text)
        log.debug(detected_markers)
        log.info('len detected text: %d' % len(detected_text))
        log.info('len detected markers: %d' % len(detected_markers))
        curr_detected_text.extend(detected_text)
        curr_detected_markers.extend(detected_markers)
        if counter % count_pages == 0:
            if len(curr_detected_text) != count_text or len(curr_detected_markers) != count_markers:
                if len(curr_detected_text) != count_text:
                    errors += '<br>Error in ' + image_file_path.split('/')[-1] + ': Incorrect no. of text boxes.' \
                              + '<br>No. of text boxes detected in image file is ' + str(len(curr_detected_text)) \
                              + ', but no. of text boxes in template file is ' + str(count_text) + '.' \
                              + '<br>Possible issues: ' + '<br>(1) Four corners of the paper in the image are not ' \
                                                          'visible, ' \
                                                          '(2) Blurred image, ' \
                              + '<br>(3) Text is written outside some text box, or ' \
                              + '<br>(4) Some random marking on paper that is causing error in text box detection.<br>'
                elif len(curr_detected_markers) != count_markers:
                    errors += '<br>Error in ' + image_file_path.split('/')[-1] + ': Incorrect no. of marker circles.' \
                              + '<br>No. of marker circles detected in image file is ' \
                              + str(len(curr_detected_markers)) \
                              + ', but no. of marker circles in template file is ' + str(count_markers) + '.' \
                              + '<br>Possible issues: ' + '<br>(1) Four corners of the paper in the image are not ' \
                                                          'visible, ' \
                              + '<br>(2) Blurred image, ' \
                              + '<br>(3) Some OMR circle has shading outside the circle, or' \
                              + '<br>(4) Some random marking on paper that is causing error in OMR circle ' \
                                'detection.<br>'
            else:
                count_correct_surveys += 1
                all_detected_text.append(curr_detected_text)
                all_detected_markers.append(curr_detected_markers)
            curr_detected_text = []
            curr_detected_markers = []
        counter += 1
    # write to excel file
    if count_correct_surveys > 0:
        excel_utils.write_to_excel_with_images(all_detected_text, all_detected_markers, template_info,
                                               output_excel_file_path)
    return errors, count_correct_surveys


if __name__ == '__main__':
    extract_omr_text(sys.argv[1], sys.argv[2])
