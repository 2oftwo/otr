# coding: utf=8
"""
Unit tests for various utilities in otr_utils.py
"""
import os
import otr_utils
import glog as log

vhl_endline_root_dir = 'tests/resources/vhl_endline'
log.setLevel(log.DEBUG)


class TestOtrUtils:
    """
    Unit tests for various utilities in otr_utils.
    """
    def test_student1_page1(self):
        _, p_image, _, _ = otr_utils.preprocess_image(os.path.join(vhl_endline_root_dir, 'student1-1.jpg'))
        text_locations, marker_locations = otr_utils.detect_text_and_omr_locations(p_image)
        assert len(marker_locations) == 104
        assert len(text_locations) == 7

    def test_student2_page1(self):
        _, p_image, _, _ = otr_utils.preprocess_image(os.path.join(vhl_endline_root_dir, 'student2-1.jpg'))
        text_locations, marker_locations = otr_utils.detect_text_and_omr_locations(p_image)
        assert len(marker_locations) == 104
        assert len(text_locations) == 7

    def test_student3_page1(self):
        _, p_image, _, _ = otr_utils.preprocess_image(os.path.join(vhl_endline_root_dir, 'student3-1.jpg'))
        text_locations, marker_locations = otr_utils.detect_text_and_omr_locations(p_image)
        assert len(marker_locations) == 104
        assert len(text_locations) == 7

    def test_student4_page1(self):
        _, p_image, _, _ = otr_utils.preprocess_image(os.path.join(vhl_endline_root_dir, 'student4-1.jpg'))
        text_locations, marker_locations = otr_utils.detect_text_and_omr_locations(p_image)
        assert len(marker_locations) == 104
        assert len(text_locations) == 7

    def test_student5_page1(self):
        _, p_image, _, _ = otr_utils.preprocess_image(os.path.join(vhl_endline_root_dir, 'student5-1.jpg'))
        text_locations, marker_locations = otr_utils.detect_text_and_omr_locations(p_image)
        assert len(marker_locations) == 104
        assert len(text_locations) == 7

    def test_student1_page2(self):
        _, p_image, _, _ = otr_utils.preprocess_image(os.path.join(vhl_endline_root_dir, 'student1-2.jpg'))
        text_locations, marker_locations = otr_utils.detect_text_and_omr_locations(p_image)
        assert len(marker_locations) == 39
        assert len(text_locations) == 3

    def test_student2_page2(self):
        _, p_image, _, _ = otr_utils.preprocess_image(os.path.join(vhl_endline_root_dir, 'student2-2.jpg'))
        text_locations, marker_locations = otr_utils.detect_text_and_omr_locations(p_image)
        assert len(marker_locations) == 39
        assert len(text_locations) == 3

    def test_student3_page2(self):
        _, p_image, _, _ = otr_utils.preprocess_image(os.path.join(vhl_endline_root_dir, 'student3-2.jpg'))
        text_locations, marker_locations = otr_utils.detect_text_and_omr_locations(p_image)
        assert len(marker_locations) == 39
        assert len(text_locations) == 3

    def test_student4_page2(self):
        _, p_image, _, _ = otr_utils.preprocess_image(os.path.join(vhl_endline_root_dir, 'student4-2.jpg'))
        text_locations, marker_locations = otr_utils.detect_text_and_omr_locations(p_image)
        assert len(marker_locations) == 39
        assert len(text_locations) == 3

    def test_student5_page2(self):
        _, p_image, _, _ = otr_utils.preprocess_image(os.path.join(vhl_endline_root_dir, 'student5-2.jpg'))
        text_locations, marker_locations = otr_utils.detect_text_and_omr_locations(p_image)
        assert len(marker_locations) == 39
        assert len(text_locations) == 3
