# coding: utf-8
"""
Utilities for extraction of optical marks and text in documents using image processing libraries.
"""
import codecs


def extract_info(template_file_path):
    """
    Read template file containing the ordering of text boxes and marker circles
    into a template_info list of tuples (x,y,z), where
    x = information category: 'T' for text, 'M' for marker, or 'M+T' for marker+text
    y = attribute name
    z = attribute value (this may be an empty string if x='M+T', and will definitely be empty if x='T')
    :param template_file_path:
    :return:
    """
    errors = ''
    f = codecs.open(template_file_path, 'rb', 'utf-8')
    template_info = []
    # read template along with error checking
    # (because of error checking, this 'for' loop has not been compressed)
    line_num = 1
    count_markers = 0
    count_text = 0
    count_pages = 1
    for line in f:
        if line.rstrip() == 'PGBRK':
            count_pages += 1
            continue
        line_split = line.rstrip().split('_')
        if len(line_split) < 3 or (line_split[0] == 'T' and len(line_split[2]) > 0) or (line_split[0] == 'M'
                                                                                        and len(line_split[2]) == 0):
            errors += '<br>Error in template file in line no: ' + str(line_num) + \
                      '. <br>Check template file specifications.'
        if line_split[0] == 'T':
            count_text += 1
        elif line_split[0] == 'M':
            count_markers += 1
        elif line_split[0] == 'M+T':
            if len(line_split[2]) == 0:
                count_text += 1
            else:
                count_markers += 1
        else:
            errors += '<br>Error in template file in line no: ' + str(line_num) + \
                      '. <br>Check template file specifications.'
        template_info.append(tuple(line_split))
        line_num += 1
    print('Template reading done: %s' % template_info)
    return template_info, count_text, count_markers, count_pages, errors
