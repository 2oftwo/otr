#!/usr/bin/env python

"""Usage: python server.py
Starts a server that displays the main web page for the OTR software and connects to all the backend logic.
"""

import glog as log
import tornado.ioloop
import tornado.web
import time
import os
from argparse import ArgumentParser
import file_utils
import otr_utils

_TEMPLATE_FIXED = True
_TEMPLATE_FILE_PATH = 'resources/vhl-endline-form.c6t'
_OUTPUT_EXCEL_FILES_DIR_PATH = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'external_files/excel_files')


class FormHandler(tornado.web.RequestHandler):
    """
    Tornado's request handler for index.html
    """

    def get(self):
        """
        Render index.html
        :return: None
        """
        self.render('web_pages/index.html')

    def post(self):
        """
        Process the POST request with input scanned documents (received as JPEG or PNG), and render the result to a
        new html page. The result will contain all the output excel files as well as any errors that were
        encountered during document processing.
        :return: None
        """
        # if necessary, create a directory to store the uploaded files
        curr_time = str(time.time()) # current time is stored to mark the input and output folders
        upload_dir_path = 'external_files/uploads/' + curr_time
        if not os.path.exists(upload_dir_path):
            os.makedirs(upload_dir_path)
        # upload image files to the server
        file_utils.upload_files(self.request.files['image_files'], upload_dir_path)
        # if template is not fixed, upload the template file to the server
        template_file_path = _TEMPLATE_FILE_PATH
        if not _TEMPLATE_FIXED:
            file_utils.upload_files(self.request.files['template_file'], 'resources')
            template_file_path = 'resources/' + self.request.files['template_file']
        # specify output excel file path and specify name based on timestamp
        # if needed, create output excel files directory
        output_excel_dir = _OUTPUT_EXCEL_FILES_DIR_PATH
        if not os.path.exists(output_excel_dir):
            os.makedirs(output_excel_dir)
        output_excel_file_name = curr_time + '.xlsx'
        output_excel_file_path = output_excel_dir + '/' + output_excel_file_name
        # get errors and the number of processed documents returned from extract_omr_text library
        errors, count_processed_docs = otr_utils.extract_omr_text(upload_dir_path, template_file_path,
                                                                  output_excel_file_path)
        # if no documents were correctly processed, don't make excel file available for download
        if count_processed_docs == 0:
            output_excel_file_name = None
        # make result excel files available for download and display errors, if applicable
        self.render('web_pages/result.html', output_excel_file_name=output_excel_file_name, errors=errors)


def make_app():
    """
    Specify Tornado application handler(s).
    :return: tornado.web.Application with all the handler(s)
    """
    return tornado.web.Application([
        (r'/otr', FormHandler),
        (r"/otr/download/(.*\.xlsx)", tornado.web.StaticFileHandler, {"path": _OUTPUT_EXCEL_FILES_DIR_PATH})
    ])


def parse_arguments():
    parser = ArgumentParser()
    parser.add_argument('--port', '-p', type=int, default=8888,
                        help='Port number on which the application will be served.')
    parser.add_argument('--debug_mode', action='store_true', default=False,
                        help='Switch on logging of debug messages.')
    return parser.parse_args()


if __name__ == '__main__':
    args = parse_arguments()
    app = make_app()
    app.listen(args.port)
    if args.debug_mode:
        log.setLevel(log.DEBUG)
    log.info('Tornado server running on port: %d' % args.port)
    tornado.ioloop.IOLoop.current().start()
