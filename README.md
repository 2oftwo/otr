# Optical Mark and Text Recognition for Survey Sheets

OTR or the Optical Mark and Text Recognition software automates the detection of optical marks and handwritten text in A4 size documents of any type. 
The document is input as an image, either scanned by a high quality scanner or taken from a good quality smartphone.

## Start a server that displays the main web page for the OTR software and connects to all the backend logic.
```
python server.py
```
